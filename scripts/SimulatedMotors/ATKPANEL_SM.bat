@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-atkpanel.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch ATKPanel
#           
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED SOLEIL_ROOT (
 ECHO SOLEIL_ROOT is not defined. Aborting!
 ECHO Please define a SOLEIL_ROOT env. var. pointing to your SOLEIL install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL %SOLEIL_ROOT%\tango\bin\common.bat 

start javaw -cp %CLASSPATH% -DTANGO_HOST=%TANGO_HOST% atkpanel.MainPanel tmp/piezogenericbender/piezogenericbender

:SCRIPT_END
