
@ECHO OFF
GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: tdbarchiver.bat
# Author:   Jean Chinkumo
# Date:     August 23 2005
# Purpose:  Launch the archiving DServer for temporary archiving
#			(Insert data into temporary database)
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED SOLEIL_ROOT (
 ECHO SOLEIL_ROOT is not defined. Aborting!
 ECHO Please define a SOLEIL_ROOT env. var. pointing to your SOLEIL install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL %SOLEIL_ROOT%\tango\bin\common.bat


set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\SimulatedMotor.jar

java -cp %CLASSPATH% -DTANGO_HOST=%TANGO_HOST% SimulatedMotor/SimulatedMotor piezogenericbender

:SCRIPT_END
        