# Microsoft Developer Studio Project File - Name="PiezoGenericBender" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=PiezoGenericBender - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "PiezoGenericBender.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "PiezoGenericBender.mak" CFG="PiezoGenericBender - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "PiezoGenericBender - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "PiezoGenericBender - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "PiezoGenericBender - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /Z7 /O2 /X /I "C:\PROGRA~1\Microsoft Visual Studio\VC98\INCLUDE" /I "..\src" /I "$(SOLEIL_ROOT)\tango\include" /I "$(SOLEIL_ROOT)\omniorb\include" /I "$(SOLEIL_ROOT)\dev\include" /I "$(SW_SUPPORT)\Exceptions\include" /I "$(SW_SUPPORT)\Interpolator\include" /I "$(SW_SUPPORT)\Utils\include" /I "$(SW_SUPPORT)\gsl\include" /I "$(SW_SUPPORT)\GenericBender\include" /I "$(SW_SUPPORT)\Monochromator\include" /D "WIN32" /D "NDEBUG" /D "INTEL86" /D "STRICT" /D "WIN32_LEAN_AND_MEAN" /D "VC_EXTRALEAN" /FR /FD /Zm1000 /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 $(SW_SUPPORT)\Interpolator\lib\static\msvc-6.0\libInterpolator.lib $(SW_SUPPORT)\Monochromator\lib\static\msvc-6.0\libMonochromator.lib $(SW_SUPPORT)\GenericBender\lib\static\msvc-6.0\libGenericBender.lib $(SW_SUPPORT)\Utils\lib\static\msvc-6.0\libUtils.lib $(SW_SUPPORT)\Exceptions\lib\static\msvc-6.0\libExceptions.lib $(SW_SUPPORT)\GSL\lib\static\msvc-6.0\libGSL.lib $(SW_SUPPORT)\GSL\lib\static\msvc-6.0\libGSLcblas.lib $(SOLEIL_ROOT)\tango\lib\mix\log4tango.lib $(SOLEIL_ROOT)\tango\lib\mix\tango.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omniORB405_rt.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omnithread30_rt.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\COS405_rt.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omniDynamic405_rt.lib kernel32.lib user32.lib gdi32.lib advapi32.lib ws2_32.lib comctl32.lib /nologo /subsystem:console /incremental:yes /machine:I386 /out:"c:\DeviceServers\ds_PiezoGenericBender.exe"
# SUBTRACT LINK32 /pdb:none /debug /nodefaultlib

!ELSEIF  "$(CFG)" == "PiezoGenericBender - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /MDd /W3 /GR /GX /Z7 /Ox /Gy /X /I "C:\PROGRA~1\Microsoft Visual Studio\VC98\INCLUDE" /I "..\src" /I "..\include" /I "$(SOLEIL_ROOT)\omniorb\include" /I "$(SOLEIL_ROOT)\tango\include" /I "$(SOLEIL_ROOT)\dev\include" /I "$(SW_SUPPORT)\Exceptions\include" /I "$(SW_SUPPORT)\Interpolator\include" /I "$(SW_SUPPORT)\Utils\include" /I "$(SW_SUPPORT)\gsl\include" /I "$(SW_SUPPORT)\GenericBender\include" /I "$(SW_SUPPORT)\Monochromator\include" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "DEBUG" /D "INTEL86" /D "STRICT" /D "WIN32_LEAN_AND_MEAN" /D "VC_EXTRALEAN" /FR /FD /Zm1000 /c
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 $(SW_SUPPORT)\Interpolator\lib\static\msvc-6.0\libInterpolatord.lib $(SW_SUPPORT)\Monochromator\lib\static\msvc-6.0\libMonochromatord.lib $(SW_SUPPORT)\GenericBender\lib\static\msvc-6.0\libGenericBenderd.lib $(SW_SUPPORT)\Utils\lib\static\msvc-6.0\libUtilsd.lib $(SW_SUPPORT)\Exceptions\lib\static\msvc-6.0\libExceptionsd.lib $(SW_SUPPORT)\GSL\lib\static\msvc-6.0\libGSLd.lib $(SW_SUPPORT)\GSL\lib\static\msvc-6.0\libGSLcblasd.lib $(SOLEIL_ROOT)\tango\lib\mix\log4tangod.lib $(SOLEIL_ROOT)\tango\lib\mix\tangod.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omniORB405_rtd.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omnithread30_rtd.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\COS405_rtd.lib $(SOLEIL_ROOT)\omniorb\lib\x86_win32\omniDynamic405_rtd.lib kernel32.lib user32.lib gdi32.lib advapi32.lib ws2_32.lib comctl32.lib /nologo /subsystem:console /debug /machine:I386 /out:"c:\DeviceServers\ds_PiezoGenericBender.exe" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "PiezoGenericBender - Win32 Release"
# Name "PiezoGenericBender - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\src\ClassFactory.cpp
# End Source File
# Begin Source File

SOURCE=..\src\main.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PiezoGenericBender.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PiezoGenericBenderClass.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PiezoGenericBenderStateMachine.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\src\PiezoGenericBender.h
# End Source File
# Begin Source File

SOURCE=..\src\PiezoGenericBenderClass.h
# End Source File
# Begin Source File

SOURCE=..\src\PiezoGenericBenderConfig.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
